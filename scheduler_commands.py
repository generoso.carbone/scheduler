#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
from os import path

#cli command: help
def print_usage():
	print "\
Usage: python scheduler.py [command]\n\
  help                          : show this message\n\
  list                          : show the list of stored histories\n\
  history <history name>        : force the scheduler to executed the specified history\n\
"
#end print_usage

#cli command: list
def cli_list():
	history_path = path.join(os.getcwd(), 'histories')

	try:
		count = 1
		for file in os.listdir(history_path):
			if file.startswith('history_'):
				print str(count) + ". " + file
				count += 1
	except IOError as e:
		print 'list error: ' + e.strerror
#end cli_list