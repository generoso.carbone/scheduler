#!/usr/bin/python
# -*- coding: utf-8 -*-

#controllo del tempo in input
def validate_time(input_time):
	if len(input_time) < 2 or len(input_time) > 6:
		time_usage("wrong time format")
		return False

	tokens = input_time.split(' ')

	if len(tokens) > 2:
		time_usage("wrong time format")
		return False

	#minutes = 1
	#hours = 0
	time_code = -1

	for t in tokens:
		minutes_or_hours = t[-1]

		if minutes_or_hours == 'h' and time_code == -1:
			time_code = 0	
		elif minutes_or_hours == 'm'and time_code != 1:
			time_code = 1
		else:
			time_usage("wrong option/format " + minutes_or_hours + ": expected h and/or m")
			return False

		time = t[0:-1]
		time_spec = t[-1]

		if not time.isdigit():
			time_usage("wrong time value: expected an integer")
			return False

		if int(time) < 1:
			time_usage("wrong time value")
			return False

		if time_spec == 'h' and int(time) > 4:
			time_usage("wrong time value: max value for hours is 4")
			return False

		if time_spec == 'm' and int(time) > 59 and int(time) < 0:
			time_usage("wrong time value between 0 and 59")
			return False

	return True
#end validate_time

#controllo url in input
def validate_url(url):
	return url.startswith("http://") or url.startswith("https://")
#end validate_url