#!/usr/bin/python
# -*- coding: utf-8 -*-

import webbrowser
import sys
import os
from os import path
from datetime import datetime, date, time
from time import sleep
import time

#scheduler imports
import scheduler_warnings
import scheduler_commands
import scheduler_validation

#absolute path for histories' folder
full_path = ''

#open browser in a new tab if possible
new = 2

#1 ora attività
timesec = 60*60

#15 minuti riposo
resttime = 15*60

#container per le attività
scheduler = []

#link per la fase di riposo
rest = 'https://www.youtube.com/watch?v=iSl1kmQMG2E'

class Activity:
	def __init__(self, activity_name, activity_url, activity_time):
		self.activity_name = activity_name
		self.activity_time = activity_time
		self.activity_url = activity_url
#end class Activity

#converto la durata in input in long (secondi)
def get_long_time(input_time):

	tokens = input_time.split(' ')	
	time = 0
	for t in tokens:
		if t[-1] == 'h':
			time += int(t[0:-1])*60*60
		else:
			time += int(t[0:-1])*60

	return time
#end get_long_time

#funzione per il riempimento dello scheduler
def fill_scheduler():
	while True:
		#inserimento durata attività o uscita dalla procedura di inserimento
		activity = raw_input('activity name: ')
		if len(activity) == 0:
			if len(scheduler) == 0:
				print "insert at least one pair <activity,url>"
				continue
			else:
				break
		#else:
		#	scheduler[activity] = ''
		#end if

		#inserimento url attività
		while True:
			url = raw_input('activity url: ')
			if scheduler_validation.validate_url(url):
				break
			else:
				scheduler_warnings.url_usage()
		#end while

		#inserimento durata attività
		while True:
			activity_time = raw_input('activity time: ')
			if scheduler_validation.validate_time(activity_time):
				break
		#end while

		scheduler.append(Activity(activity, url, get_long_time(activity_time)))
	#end while
#end fill_scheduler

def write_history():
	history_name = 'history_' + datetime.today().strftime('%Y%m%d%H%M')
	
	history_object = open(path.join(full_path, history_name), "w")

	for a in scheduler:
		string_activity = a.activity_name+"\n\t"+a.activity_url+"\n\t"+str(a.activity_time)
		history_object.write(string_activity)

	history_object.close()

def command_line_interpreter():
	if len(sys.argv) == 1:
		#riempimento dello scheduler
		fill_scheduler()

		#salvo i dati dello scheduler su disco
		write_history()

		return
	#end if

	if len(sys.argv) == 2:
		if sys.argv[-1] == 'help':
			scheduler_commands.print_usage()
		elif sys.argv[-1] == 'list':
			scheduler_commands.cli_list()
		elif sys.argv[-1] == 'history':
			scheduler_warnings.print_wrong_format('history')
		else:
			scheduler_warnings.print_wrong_command(sys.argv[-1])
		return
	#end if

	if len(sys.argv) == 3:
		if sys.argv[1] == 'help'or sys.argv[1] == 'list':
			scheduler_warnings.print_wrong_format(sys.argv[1])
		elif sys.argv[1] == 'history':
			scheduler_warnings.print_not_implemented('history')
		else:
			scheduler_warnings.print_wrong_command(sys.argv[1])
		return
	#end if

	scheduler_warnings.print_wrong_command(sys.argv[1])
	return
#end command_line_interpreter

#This method checks if the path for the scheduler folder exists.
#If don't, it creates it.
def check_path():
	global full_path
	full_path = path.normpath(path.join(os.getcwd(), 'histories'))
	if path.exists(full_path):
		return True

	try:
		os.mkdir(full_path, 0755);
	except OSError as e:
		print 'Cannot create folder ' + full_path +	': ' + e.strerror;
		return False

	return True


#inizio esecuzione dello script
print "\
\
    SCHEDULER 0.4 by Generoso Carbone \
\
"
#check paths
if not check_path():
	print 'Exit...'
	exit()

#read command-line arguments
command_line_interpreter()

for activity in scheduler:
	print "start activity " + activity.activity_name + " at time " + datetime.ctime(datetime.today());

	webbrowser.open(activity.activity_url,new=new)
	sleep(activity.activity_time)

	print "start rest at time " + datetime.ctime(datetime.today());
	webbrowser.open(rest,new=new)	
	sleep(resttime)	

if len(scheduler) > 0:
	print 'tutto ok per oggi'

# Quello che si vuole fare ora è creare uno scheduler vero e proprio.
# 1. Opzione per avviare lo scheduler sfruttando l'history
#	 @Lettura file
#	 @Argomenti riga di comando
#
# 2. Meccanismo per la memorizzazione delle