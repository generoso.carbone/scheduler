#!/usr/bin/python
# -*- coding: utf-8 -*-
import scheduler_commands

def time_usage(message):
	print message
	print "\twhere t = 1,..., 4 and h means hours"
	print "\t\tOR"
	print "\twhere t = 1,..., 59 and m means minutes"	

def url_usage():
	print "url should start with http:// or https://"

def print_not_implemented(command):
	print str(command) + " not implemented"
#end print_not_implemented

def print_wrong_command(command):
	print "wrong command: " + str(command)
	scheduler_commands.print_usage()
#end print_wrong_command

def print_wrong_format(command):
	print "wrong format for command: " + str(command)
	scheduler_commands.print_usage()
#end print_wrong_command